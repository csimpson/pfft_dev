from django.conf.urls import patterns, url

urlpatterns = patterns('',
                       url(r'^calendar$', 'princess_calendar.views.icalendar', name='icalendar'),
)
