from django.db import models
from datetime import datetime


class Cinema(models.Model):
    name = models.CharField(max_length=512, null=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s' %(self.name)


class Screen(models.Model):
    name = models.CharField(max_length=512, null=True)
    cinema = models.ForeignKey('Cinema')
    added_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s - %s' %(self.cinema, self.name)

class Film(models.Model):
    url = models.URLField(max_length=2048, unique=True)
    title = models.CharField(max_length=1024)
    duration = models.PositiveSmallIntegerField(default=0)
    added_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s' %(self.title)


class Screening(models.Model):
    url = models.URLField(max_length=2048, unique=True)
    film = models.ForeignKey('Film', null=True)
    start_time = models.DateTimeField(default=datetime.now())
    end_time = models.DateTimeField(default=datetime.now())
    screen = models.ForeignKey('Screen', null=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s @ %s' %(self.film, self.start_time)
