# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Film', fields ['url']
        db.create_unique(u'princess_calendar_film', ['url'])

        # Adding unique constraint on 'Screening', fields ['url']
        db.create_unique(u'princess_calendar_screening', ['url'])


    def backwards(self, orm):
        # Removing unique constraint on 'Screening', fields ['url']
        db.delete_unique(u'princess_calendar_screening', ['url'])

        # Removing unique constraint on 'Film', fields ['url']
        db.delete_unique(u'princess_calendar_film', ['url'])


    models = {
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']