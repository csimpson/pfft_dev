# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cinema'
        db.create_table(u'princess_calendar_cinema', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=512, null=True)),
        ))
        db.send_create_signal(u'princess_calendar', ['Cinema'])

        # Adding model 'Screen'
        db.create_table(u'princess_calendar_screen', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=512, null=True)),
            ('cinema', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['princess_calendar.Cinema'])),
        ))
        db.send_create_signal(u'princess_calendar', ['Screen'])

        # Adding field 'Screening.screen'
        db.add_column(u'princess_calendar_screening', 'screen',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['princess_calendar.Screen'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Cinema'
        db.delete_table(u'princess_calendar_cinema')

        # Deleting model 'Screen'
        db.delete_table(u'princess_calendar_screen')

        # Deleting field 'Screening.screen'
        db.delete_column(u'princess_calendar_screening', 'screen_id')


    models = {
        u'princess_calendar.cinema': {
            'Meta': {'object_name': 'Cinema'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'})
        },
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            'duration': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        },
        u'princess_calendar.screen': {
            'Meta': {'object_name': 'Screen'},
            'cinema': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Cinema']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 29, 0, 0)'}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'screen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Screen']", 'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 29, 0, 0)'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']