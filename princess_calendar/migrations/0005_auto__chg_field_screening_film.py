# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Screening.film'
        db.alter_column(u'princess_calendar_screening', 'film_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['princess_calendar.Film'], null=True))

    def backwards(self, orm):

        # Changing field 'Screening.film'
        db.alter_column(u'princess_calendar_screening', 'film_id', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['princess_calendar.Film']))

    models = {
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']