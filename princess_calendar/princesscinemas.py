import requests
import re
from icalendar import Calendar
from lxml import html
from urlparse import urlparse, urlunparse
from dateutil.relativedelta import relativedelta
from models import Screening, Film, Cinema, Screen


def get_original_icalendar_content(url):
    r = requests.get(url)
    return r.content


def modify_all_events(calendar):
    for subcomponent in calendar.subcomponents:
        if subcomponent.name == 'VEVENT':
            modify_event(subcomponent)


def modify_event(event):
    screening = get_screening_from_event_body(event)
    if screening.film is not None:
        event['SUMMARY'] = screening.film.title
    event['DTEND'].dt = screening.end_time
    event['LOCATION'] = screening.screen


def duration_text_to_int(duration_text):
    digit_groups = re.findall('\\d+', duration_text)
    if len(digit_groups) == 1:
        return int(digit_groups[0])
    return 0


def get_film_duration(film_url):
    html_doc = html.parse(film_url)
    root = html_doc.getroot()
    node_set = root.cssselect('.field-running-time .field-item')
    if len(node_set) == 1:
        node_content = node_set[0].getchildren()
        if node_content is not None:
            if len(node_content) == 1:
                duration_text = node_content[0].tail
                return duration_text_to_int(duration_text)
    return 120


def get_node(root, css_class):
    node_set = root.cssselect(css_class)
    if len(node_set) >= 1:
        node_content = node_set[0]
        return node_content
    return None


def get_text_from_node(node):
    if node is not None:
        return node.text.strip()
    return None


def get_info_from_event_page(event_url):
    result = {}
    html_doc = html.parse(event_url)
    root = html_doc.getroot()

    result['event_url'] = event_url
    result['cinema_name'] = get_text_from_node(get_node(root, '.field-movie-cinema .field-item'))
    result['screen_name'] = get_text_from_node(get_node(root, '.field-movie-cinema-screen .field-item'))
    result['title_node'] = get_node(root, '.field-movie-ref .field-item a')
    if result['title_node'] is None:
        result['title_node'] = get_node(root, 'h1.title')

    return result


def get_film(absolute_film_url, film_title):
    film, created_film = Film.objects.get_or_create(url=absolute_film_url)
    if created_film:
        film.title = film_title
        film.duration = get_film_duration(absolute_film_url)
        film.save()
    return film


def construct_absolute_film_url(event_page_info, parsed_film_url):
    parsed_event_url = urlparse(event_page_info['event_url'])
    url_pieces = [parsed_event_url.scheme, parsed_event_url.netloc]
    url_pieces.extend(parsed_film_url[2:])
    absolute_film_url = urlunparse(url_pieces)
    return absolute_film_url


def get_film_url(event_page_info):
    if 'href' in event_page_info['title_node'].attrib:
        film_url = event_page_info['title_node'].attrib['href']
    else:
        film_url = event_page_info['event_url']
    parsed_film_url = urlparse(film_url)
    if not parsed_film_url.netloc:
        absolute_film_url = construct_absolute_film_url(event_page_info, parsed_film_url)
    else:
        absolute_film_url = film_url
    return absolute_film_url


def get_film_basic_details(event_page_info):
    film_title = event_page_info['title_node'].text
    film_url = get_film_url(event_page_info)
    return film_url, film_title


def get_screen(event_page_info):
    cinema_name = event_page_info['cinema_name']
    cinema, created_cinema = Cinema.objects.get_or_create(name=cinema_name)
    screen_name = event_page_info['screen_name']
    screen, created_screen = Screen.objects.get_or_create(name=screen_name, cinema=cinema)
    return screen


def save_screening_details(event, screening):
    event_url = event['URL']
    event_page_info = get_info_from_event_page(event_url)
    title_node = event_page_info['title_node']
    if title_node is None:
        screening.film = None
    else:
        absolute_film_url, film_title = get_film_basic_details(event_page_info)
        film = get_film(absolute_film_url, film_title)
        screening.film = film
        screening.start_time = event['DTSTART'].dt
        if screening.film:
            duration = screening.film.duration
            time_delta = relativedelta(minutes=duration)
            start_time = screening.start_time
            end_time = start_time + time_delta
            screening.end_time = end_time
        else:
            screening.end_time = screening.start_time

    screening.screen = get_screen(event_page_info)
    screening.save()


def get_screening_from_event_body(event):
    event_url = event['URL']
    screening, created_screening = Screening.objects.get_or_create(url=event_url)
    if created_screening:
        save_screening_details(event, screening)

    return screening


def get_modified_calendar():
    original_calendar = get_original_icalendar_content('http://www.princesscinemas.com/bytowne.ics')
    calendar = Calendar.from_ical(st=original_calendar)
    modify_all_events(calendar)
    return calendar.to_ical()
