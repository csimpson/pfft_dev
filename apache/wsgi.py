"""
WSGI config for PFFT project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""
import os
import sys

app_path = '/usr/local/src/jmathews/pfft'
if app_path not in sys.path:
    sys.path.append(app_path)

print sys.path

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "PFFT.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

